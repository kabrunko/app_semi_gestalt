# Anotações

* Scholars have also examined how social elements such as pictures of people or emotive text on websites empirically impact users’ impressions such as enjoyment (Cyr et al. 2006; Gefen and Staub 2003; Hassanein and Head, 2007);

* Further research has aimed to develop models that incorporate hedonic elements. Related to affect, Loiacono and Djamasbi (2010) proposed the relevance of mood (such as sadness, fear, or happiness) for system usage models that could be applied online. They further outlined a model in which mood is intended to influence perception, evaluation, and cognitive effort resulting in variable levels of IS usage behavior. While this model is not tested, it is a useful framework from which to investigate emotion empirically. 

<!-- 
- ICONS
    > Ícone do gato, na seção sobre (https://iconmonstr.com/cat-3-svg/)
-->